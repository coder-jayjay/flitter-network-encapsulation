import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/config/app_colors.dart';

class RootNavHeader extends StatefulWidget {
  const RootNavHeader({Key? key}) : super(key: key);

  @override
  _RootNavHeaderState createState() => _RootNavHeaderState();
}

class _RootNavHeaderState extends State<RootNavHeader> {
  @override
  Widget build(BuildContext context) {
    return Container(child: Row(
      children: [
        Image.asset(
          "assets/images/icons/nav_logo.png",
          height: 40,
        ),

        Expanded(child: _searchView()),
        Image.asset(
          "assets/images/icons/nav_more.png",
           width: 16,
           height: 16,
        )

      ],
    ),);
  }

  Widget _searchView() {
     return Container(
        height: 30,
        margin: EdgeInsets.symmetric(horizontal: 15),
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(30),color: AppColors.page),
        child:Row(
          children: [
            Padding(padding: EdgeInsets.only(left: 8,right: 15),child: Image.asset("assets/images/icons/nav_search.png",width: 16,height: 16,)),
            Text("搜索关键字",style: TextStyle(color: AppColors.un3active,fontSize: 12),),
          ],

        ) ,

     );
  }
}
