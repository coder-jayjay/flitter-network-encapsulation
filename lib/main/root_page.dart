import 'package:flutter/material.dart';
import 'package:flutter_app/config/app_colors.dart';
import 'package:flutter_app/home/homePage.dart';
import 'package:flutter_app/config/app_theme.dart';
import 'package:flutter_app/home/marketPage.dart';
import 'package:flutter_app/home/publishPage.dart';
import 'package:flutter_app/home/shopCartPage.dart';
import 'package:flutter_app/home/mePage.dart';
class TabBarController extends StatefulWidget {
  const TabBarController({Key? key}) : super(key: key);

  @override
  _TabBarControllerState createState() => _TabBarControllerState();
}
const Map _tabbarNames = {
  "home":"首页",
  "market":"商场",
  "publish":"",
  "shopcart":"购物车",
  "me":"会员",
};


class _TabBarControllerState extends State<TabBarController> {
  List  <BottomNavigationBarItem> _tabbarItems = [];
  List <Widget> _contentPages = [
    HomePage(),
    MarketPage(),
    Container(),
    ShopCartPage(),
    MePage(),
  ];

  int currentIndex = 0;
  void _tabbarClick (int index){
        setState(() {
          currentIndex = index;
        });
        if(index == 2){
          _createMedio();
        }
  }

  @override
  void initState() {
    super.initState();
    _tabbarNames.forEach((key, value) {
      BottomNavigationBarItem item = _creatBarItem(key, value);
      _tabbarItems.add(item);
    });
  }

  BottomNavigationBarItem _creatBarItem(String key, String value){
    return BottomNavigationBarItem(
      icon: Image.asset(
        "assets/images/icons/$key.png",
        width: 24,
        height: 24,
      ),
      activeIcon: Image.asset(
        "assets/images/icons/${key}.active.png",
        width: 24,
        height: 24,
      ),
      label: value,
      tooltip: "",
    );
  }

  @override
  Widget build(BuildContext context) {

    Widget  _publishBtn = Container(
      color: AppColors.page,
      margin: EdgeInsets.only(top: 56),
      width: 48,
      height: 48,
      child: FloatingActionButton(
          child: Image.asset(
            "assets/images/icons/publish.png",
          ),
          onPressed: (){
            _createMedio();
          }),
    );

    return Scaffold(
      floatingActionButton: _publishBtn,
      floatingActionButtonLocation: FloatingActionButtonLocation.miniCenterDocked,
      body:_contentPages[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed ,
        currentIndex: currentIndex,
        items: _tabbarItems,
        onTap: _tabbarClick,
      )

    );
  }

  void _createMedio() {

    print("创建视频");
  }

  String say(String from, String msg, [String? device]) {
    var result = '$from says $msg';
    if (device != null) {
      result = '$result with a $device';
    }
    return result;
  }
}

