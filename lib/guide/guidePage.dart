import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_app/main/root_page.dart';

class guidePage extends StatefulWidget {


  const guidePage({Key? key}) : super(key: key);

  @override
  _guidePageState createState() => _guidePageState();
}

class _guidePageState extends State<guidePage> {
  late Timer _timer;
  int _currentTimes = 3;
  @override
  void initState() {
    super.initState();
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        _currentTimes --;
      });
      if(_currentTimes <=0){
        _jumpToHome();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body:Stack(
      children: [
        Image.asset(
          'assets/images/common/launch.png',
          fit: BoxFit.cover,
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
        ),
        Positioned(
          top: MediaQuery.of(context).padding.top + 10,
          right: 10,
          width: 50,
          height: 50,
          child: InkWell(
            child: _jumpButton(),
            onTap: _jumpToHome,
          ),
        )
      ],
    ));
  }

  Widget _jumpButton(){
    return ClipRRect(
      borderRadius: BorderRadius.circular(50),
      child: Container(
        width: 50,
        height: 50,
        color: Colors.black.withOpacity(0.5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("跳过",style: TextStyle(color: Colors.white,fontSize: 12),),
            Text( "${_currentTimes}s",style: TextStyle(color: Colors.white,fontSize: 12))
          ],
        ),
      ),
    );
  }

  void _jumpToHome() {
      _timer.cancel();
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => TabBarController()), (route) => false);
  }
}



