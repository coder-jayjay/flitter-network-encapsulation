import 'package:flutter/material.dart';
import 'package:flutter_app/guide/guidePage.dart';
import 'package:flutter_app/config/app_theme.dart';

void main() {
  runApp(MyApp());

}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: myThemeData.theme,
      debugShowCheckedModeBanner: false,
      home: Container(
        color: Colors.amber,
        child: guidePage(),
      )
    );
  }
}

