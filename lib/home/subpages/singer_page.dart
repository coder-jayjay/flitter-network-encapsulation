import 'dart:ffi';

import 'package:flutter/material.dart';


class SingerPage extends StatefulWidget  {
  const SingerPage({Key? key}) : super(key: key);

  @override
  _SingerPageState createState() => _SingerPageState();


}
///保持不刷新1。 三部曲
class _SingerPageState extends State<SingerPage>  with AutomaticKeepAliveClientMixin{


  @override
  // TODO: implement wantKeepAlive
  ///保持不刷新2。
  bool get wantKeepAlive => true;

  final List <Widget> _itemList = [];
  @override
  void initState() {
    super.initState();
    for(int i = 0 ; i<20 ; i++){
      Widget item = _getItemContainer('$i');
      _itemList.add(item);
    };

  }
  @override
  Widget build(BuildContext context) {
    ///保持不刷新3。
    super.build(context);

    return _gridViewCustom();
  }

  //方式一
  GridView _gridView (){
    return GridView(gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
      mainAxisSpacing: 8,
      crossAxisSpacing:1,
      crossAxisCount:2,
      childAspectRatio: 1/1.5,
    ),
      children: _itemList,
      padding: EdgeInsets.only(top: 8),
    );
  }
  //方式二
  GridView _gridViewCount (){
    return GridView.count(
      crossAxisCount: 2,
      children: _itemList,
      mainAxisSpacing: 8,
      crossAxisSpacing:1,
      childAspectRatio: 1/1.5,
      padding: EdgeInsets.only(top: 8),
    );
  }
  //方式三
  GridView _gridViewBuild (){
    return GridView.builder(
      // gridDelegate:SliverGridDelegateWithFixedCrossAxisCount(
      //   crossAxisCount: 3,
      //   mainAxisSpacing: 8,
      //   crossAxisSpacing: 1,
      //   childAspectRatio: 2/3,
      //
      // ) ,
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        //单个子Widget的水平最大宽度
          maxCrossAxisExtent: 50,
          //水平单个子Widget之间间距
          mainAxisSpacing: 1.0,
          //垂直单个子Widget之间间距
          crossAxisSpacing: 1.0
      ),
      itemBuilder: (context,index){
        return _getItemContainer('$index');
      },
      itemCount: _itemList.length,
      padding: EdgeInsets.only(top: 8),
    );
  }
  //方式四
  GridView _gridViewCustom (){
    return GridView.custom(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        mainAxisSpacing: 10.0,
        crossAxisSpacing: 20.0,
      ),
      childrenDelegate: SliverChildBuilderDelegate(
        (context , index){
            return _getItemContainerIndexAndTitle(index,'$index');
        },
        childCount: _itemList.length,
      ),
      padding: EdgeInsets.only(top: 8),
    );
  }

  Widget _getItemContainer(String item) {
    return Container(
      alignment: Alignment.center,
      child: Center(child: Text(
        item,
        textAlign: TextAlign.center,
        style: TextStyle(color: Colors.white, fontSize: 20 ),
      )),
      color: Colors.orange,
    );
  }

  Widget _getItemContainerIndexAndTitle(int index,String item) {
    double height = index.isEven ? 150.00:200.00;
    return Container(
      height: height,
      alignment: Alignment.center,
      child: Center(child: Text(
        item,
        textAlign: TextAlign.center,
        style: TextStyle(color: Colors.white, fontSize: 20 ),
      )),
      color: Colors.orange,
    );
  }
}
