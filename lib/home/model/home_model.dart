/**
 * {stationId: 86, stationName: 功率分配, stationStatus: BUSY, latitude: 22.567926, longitude: 114.013849},
 *
 * */
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

class StationModelList{
   List <StationModel> list = [];
  StationModelList(this.list);

  factory StationModelList.fromJsonList(List <dynamic> jsonList){
   return StationModelList(jsonList.map((json) => StationModel.fromJson(json)).toList());
  }

}

class StationModel{
  final String stationName;
  final String stationStatus;
  final int stationId;
  final double latitude;
  final double longitude;

  StationModel({
    required this.stationId,
    required this.stationName,
    required this.stationStatus,
    required this.latitude ,
    required this.longitude});

  factory StationModel.fromJson(Map <String , dynamic> json){
    return StationModel(
        stationId:json['stationId'],
        stationName: json['stationName'],
        stationStatus: json['stationStatus'],
        latitude: json['latitude'],
        longitude:json['longitude']);

  }
}