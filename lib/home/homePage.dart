import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/home/subpages/singer_page.dart';
import 'package:flutter_app/main/root_page.dart';
import 'package:flutter_app/main/root_nav_header.dart';
import 'package:flutter_app/http/http.dart';
import 'package:flutter_app/home/model/home_model.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}
const List <Tab>_tabs = [
  Tab(text: "读书",),
  Tab(text: "推荐",),
  Tab(text: "歌曲",),
  Tab(text: "歌手",),
  Tab(text: "小视频",),
  Tab(text: "新闻",),
  Tab(text: "漫画",),
  Tab(text: "文章",)
];
const List <Widget>_tabPages = [
  Text( "读书",),
  Text( "推荐",),
  Text( "歌曲",),
  SingerPage(),
  Text( "小视频",),
  Text("新闻"),
  Text("漫画"),
  Text("文章")
];


class _HomePageState extends State<HomePage> {
  TabBarController _tabbarVc = TabBarController();
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // final String kUpgradeUrl = "user/upgrade";
    // params = @{@"packageName":@"cn.enplus.chargeout",@"platformType":@"IOS"};

    _getList();
  }

  Future _getList() async{
    String kStationNearbyUrl = "station/nearby";
    final result = await Http.get(kStationNearbyUrl);

    print('首页网络请求测试___$result');
    StationModelList stationModelList = StationModelList.fromJsonList(result['data']);
    print('首页网络请求测试model___${stationModelList.list}');
    return result;
  }
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 1,
      length: _tabs.length,
      child: Scaffold(
        appBar: AppBar(
          title: RootNavHeader(),
          bottom: PreferredSize(
            preferredSize: Size(double.infinity ,44),
            child:Container(
              width: MediaQuery.of(context).size.width,
              color: Colors.black12,
              child:TabBar(
                isScrollable: true,
                tabs: _tabs,
              ),
            ) ,

          ),
        ),
        body: TabBarView(
          children: _tabPages,
        ),
      ),
    );
  }
}

