import 'package:dio/dio.dart';
import 'package:flutter_app/http/http_exception.dart';

class HttpInterceptor extends Interceptor {
  //请求拦截
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    // TODO: implement onRequest
    super.onRequest(options, handler);
  }
  //响应拦截
  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    // TODO: implement onResponse
    super.onResponse(response, handler);
  }

  //异常拦截
  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    // TODO: implement onError
    HttpException httpException = HttpException.create(err);
    super.onError(err, handler);
  }
}